<?php
/**
 * Created by PhpStorm.
 * User: Qosmio_X770-10X
 * Date: 2018.09.10.
 * Time: 12:25
 */

class Eredmeny extends Db
{
    protected $id;
    protected $hazai_csapat_id;
    protected $vendeg_csapat_id;
    protected $hazai_golok_szama;
    protected $vendeg_golok_szama;

    public function test(){
        var_dump(get_object_vars($this));
    }

    /**
     * Eredmeny constructor.
     */
    public function __construct()
    {
        $this->db_tablename = "eredmeny";
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getHazaiCsapatId()
    {
        return $this->hazai_csapat_id;
    }

    /**
     * @param mixed $hazai_csapat_id
     */
    public function setHazaiCsapatId($hazai_csapat_id)
    {
        $this->hazai_csapat_id = $hazai_csapat_id;
    }

    /**
     * @return mixed
     */
    public function getVendegCsapatId()
    {
        return $this->vendeg_csapat_id;
    }

    /**
     * @param mixed $vendeg_csapat_id
     */
    public function setVendegCsapatId($vendeg_csapat_id)
    {
        $this->vendeg_csapat_id = $vendeg_csapat_id;
    }

    /**
     * @return mixed
     */
    public function getHazaiGolokSzama()
    {
        return $this->hazai_golok_szama;
    }

    /**
     * @param mixed $hazai_golok_szama
     */
    public function setHazaiGolokSzama($hazai_golok_szama)
    {
        $this->hazai_golok_szama = $hazai_golok_szama;
    }

    /**
     * @return mixed
     */
    public function getVendegGolokSzama()
    {
        return $this->vendeg_golok_szama;
    }

    /**
     * @param mixed $vendeg_golok_szama
     */
    public function setVendegGolokSzama($vendeg_golok_szama)
    {
        $this->vendeg_golok_szama = $vendeg_golok_szama;
    }

}
