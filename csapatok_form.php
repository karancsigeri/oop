<?php
/**
 * Created by PhpStorm.
 * User: Qosmio_X770-10X
 * Date: 2018.09.19.
 * Time: 10:44
 */

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<h1>Vidd fel adataid!</h1>
<form method="post" action="csapatok_controller.php">
    <div class="csapatok">
        <label for="csapat_nev">Csapat neve:</label>
        <input type="text" id="csapat_nev" name="csapat_nev"/>
        <br>
        <label for="tulajdonos">Csapat tulajdonosa:</label>
        <input type="text" id="tulajdonos" name="tulajdonos"/>
        <br>
        <label for="cimer">Csapat címere:</label>
        <input type="text" id="cimer" name="cimer"/>
        <br>
        <label for="becenev">Csapat beceneve:</label>
        <input type="text" id="becenev" name="becenev"/>
        <br>
        <label for="bajnok_id">Bajnokság azonosítója:</label>
        <input type="number" min="0" id="bajnok_id" name="bajnok_id"/>
    </div>
    <br>
    <input type="submit" value="Feltöltés!" id="kuld" name="feltolt"/>
    <br>
</body>
</html>
