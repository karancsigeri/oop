<?php
/**
 * Created by PhpStorm.
 * User: Qosmio_X770-10X
 * Date: 2018.09.19.
 * Time: 10:47
 */

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<h1>Vidd fel adataid!</h1>
<form method="post" action="jatekosok_controller.php">
    <div class="jatekosok">
        <label for="jatekos_nev">Játékos neve:</label>
        <input type="text" id="jatekos_nev" name="jatekos_nev"/>
        <br>
        <label for="csapat_id">Csapat azonosítója:</label>
        <input type="number" min="0" id="csapat_id" name="csapat_id"/>
        <br>
    </div>
    <br>
    <br>
    <input type="submit" value="Feltöltés!" id="kuld" name="feltolt"/>
    <br>
</body>
</html>
