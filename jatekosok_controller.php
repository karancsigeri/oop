<?php
/**
 * Created by PhpStorm.
 * User: Qosmio_X770-10X
 * Date: 2018.09.19.
 * Time: 9:28
 */

header('Content-Type: text/html; charset=utf-8');

include "autoload.php";
$new_object_link = '/oop/jatekosok_form.php';
$new_object_btn_name = 'Új játékos felvétele';
/*
$_POST['jatekos_nev'] = "";
$_POST['csapat_id'] = "";
$_POST['up_jatekos_nev']="";
$_POST['up_csapat_id']="";
*/
$jatekos = new Jatekosok();
$jatekos_adatai = [
    'nev' => $_POST['jatekos_nev'],
    'csapat_id' => $_POST['csapat_id'],
];
$jatekos->save($jatekos_adatai);


$up_object_link = '/oop/jatekosok_update_form.php';
$up_object_btn_name = 'Szerkeszt';

$up_jatekos = new Jatekosok();
$up_jatekos_adatai = [
    'nev' => $_POST['up_jatekos_nev'],
    'csapat_id' => $_POST['up_csapat_id'],
];
$up_jatekos->update($up_jatekos_adatai);


$crud_list = $jatekos->getList();
$crud_list = $up_jatekos->getList();
include "crud_table.php";
