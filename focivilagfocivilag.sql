-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Gép: 127.0.0.1
-- Létrehozás ideje: 2018. Sze 14. 08:51
-- Kiszolgáló verziója: 10.1.29-MariaDB
-- PHP verzió: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `focivilag`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `bajnoksag`
--

CREATE TABLE `bajnoksag` (
  `id` int(10) NOT NULL,
  `nev` varchar(128) DEFAULT NULL,
  `orszag` varchar(128) DEFAULT NULL,
  `szint` smallint(10) DEFAULT NULL,
  `foszponzor` varchar(128) DEFAULT NULL,
  `elnok` varchar(128) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `bajnoksag`
--

INSERT INTO `bajnoksag` (`id`, `nev`, `orszag`, `szint`, `foszponzor`, `elnok`, `is_active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Loki', 'Magyaro', 1, 'TEVA', 'Kis Pista', 1, '2018-09-11 17:14:36', NULL, NULL),
(2, 'Loki', 'Magyaro', 1, 'TEVA', 'Kis Pista', 1, '2018-09-11 17:15:16', NULL, NULL),
(3, 'Loki', 'Magyaro', 1, 'TEVA', 'Kis Pista', 1, '2018-09-11 17:21:40', NULL, NULL),
(4, 'Loki', 'Magyaro', 1, 'TEVA', 'Kis Pista', 1, '2018-09-11 17:21:51', NULL, '2018-09-10 22:00:00'),
(5, 'Loki', 'Magyaro', 1, 'TEVA', 'Kis Pista', 1, '2018-09-11 17:27:13', NULL, NULL);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `csapatok`
--

CREATE TABLE `csapatok` (
  `id` int(10) NOT NULL,
  `nev` varchar(128) NOT NULL,
  `tulajdonos` varchar(128) NOT NULL,
  `cimer` varchar(128) NOT NULL,
  `becenev` varchar(128) NOT NULL,
  `bajnoksag_id` int(10) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `insert_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` timestamp NULL DEFAULT NULL,
  `delete_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `eredmeny`
--

CREATE TABLE `eredmeny` (
  `id` int(11) NOT NULL,
  `hazai_csapat_id` int(10) NOT NULL,
  `vendeg_csapat_id` int(10) NOT NULL,
  `hazai_golok_szama` tinyint(2) NOT NULL,
  `vendeg_golok_szama` tinyint(2) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `insert_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` timestamp NULL DEFAULT NULL,
  `delete_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `eredmeny`
--

INSERT INTO `eredmeny` (`id`, `hazai_csapat_id`, `vendeg_csapat_id`, `hazai_golok_szama`, `vendeg_golok_szama`, `is_active`, `insert_at`, `update_at`, `delete_at`) VALUES
(1, 1, 2, 1, 8, 0, '2018-09-12 18:44:16', NULL, NULL),
(2, 1, 2, 1, 8, 0, '2018-09-12 18:44:16', NULL, NULL);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `jatekosok`
--

CREATE TABLE `jatekosok` (
  `id` int(10) NOT NULL,
  `nev` varchar(128) NOT NULL,
  `csapat_id` int(10) NOT NULL,
  `is_active` tinyint(4) NOT NULL,
  `insert_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` timestamp NULL DEFAULT NULL,
  `delete_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `bajnoksag`
--
ALTER TABLE `bajnoksag`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `csapatok`
--
ALTER TABLE `csapatok`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `eredmeny`
--
ALTER TABLE `eredmeny`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `jatekosok`
--
ALTER TABLE `jatekosok`
  ADD PRIMARY KEY (`id`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `bajnoksag`
--
ALTER TABLE `bajnoksag`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT a táblához `csapatok`
--
ALTER TABLE `csapatok`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `eredmeny`
--
ALTER TABLE `eredmeny`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT a táblához `jatekosok`
--
ALTER TABLE `jatekosok`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
