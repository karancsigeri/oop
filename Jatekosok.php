<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2018. 08. 28.
 * Time: 18:33
 */

class Jatekosok extends Db
{
    protected $id;
    protected $nev;
    protected $csapat_id;




    /**
     * Jatekosok constructor.
     */
    public function __construct()
    {
        $this->db_tablename = "jatekosok";
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNev()
    {
        return $this->nev;
    }

    /**
     * @param mixed $nev
     */
    public function setNev($nev)
    {
        $this->nev = $nev;
    }

    /**
     * @return mixed
     */
    public function getCsapatId()
    {
        return $this->csapat_id;
    }

    /**
     * @param mixed $csapat_id
     */
    public function setCsapatId($csapat_id)
    {
        $this->csapat_id = $csapat_id;
    }



}