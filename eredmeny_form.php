<?php
/**
 * Created by PhpStorm.
 * User: Qosmio_X770-10X
 * Date: 2018.09.19.
 * Time: 10:39
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<h1>Vidd fel adataid!</h1>
<form method="post" action="eredmeny_controller.php">
    <div class="eredmenyek">
        <label for="hazai_id">Hazai csapat azonosítója:</label>
        <input type="number" min="0" id="hazai_id" name="hazai_id"/>
        <br>
        <label for="vendeg_id">Vendég csapat azonosítója:</label>
        <input type="number" min="0" id="vendeg_id" name="vendeg_id"/>
        <br>
        <label for="hazai_gol">Hazai gólok száma:</label>
        <input type="number" min="0" id="hazai_gol" name="hazai_gol"/>
        <br>
        <label for="vendeg_gol">Vendég gólok száma:</label>
        <input type="number" min="0" id="vendeg_gol" name="vendeg_gol"/>
        <br>
    </div>
    <br>
    <br>
    <input type="submit" value="Feltöltés!" id="kuld" name="feltolt"/>
    <br>
</body>
</html>