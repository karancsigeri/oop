<?php
/**
 * Created by PhpStorm.
 * User: Qosmio_X770-10X
 * Date: 2018.09.19.
 * Time: 9:46
 */

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<h1>Vidd fel adataid!</h1>
<form method="post" action="bajnoksag_controller.php">
    <div class="bajnoksag">
        <label for="bajnok_nev">Bajnokság neve:</label>
        <input type="text" id="bajnok_nev" name="bajnok_nev"/>
        <br>
        <label for="orszag">Bajnokság országa</label>
        <input type="text" id="orszag" name="orszag"/>
        <br>
        <label for="szint">Bajnokság szintje:</label>
        <input type="number" min="0" id="szint" name="szint"/>
        <br>
        <label for="szponzor">Bajnokság főszponzora:</label>
        <input type="text" id="szponzor" name="szponzor"/>
        <br>
        <label for="elnok">Bajnokság elnöke:</label>
        <input type="text" id="elnok" name="elnok"/>
    </div>

    <br>
    <br>
    <input type="submit" value="Feltöltés!" id="kuld" name="feltolt"/>
    <br>
</body>
</html>
