<?php
/**
 * Created by PhpStorm.
 * User: Qosmio_X770-10X
 * Date: 2018.09.19.
 * Time: 9:27
 */

header('Content-Type: text/html; charset=utf-8');

include "autoload.php";
$new_object_link = '/oop/csapatok_form.php';
$new_object_btn_name = 'Új csapat felvétele';
/*
$_POST['csapat_nev'] = "";
$_POST['tulajdonos'] = "";
$_POST['cimer'] = "";
$_POST['becenev'] = "";
$_POST['bajnok_id'] = "";

$_POST['up_csapat_nev']="";
$_POST['up_tulajdonos']="";
$_POST['up_cimer']="";
$_POST['up_becenev']="";
$_POST['up_bajnok_id']="";
*/
$csapat = new Csapatok();
$csapat_adatai = [
    'nev' => $_POST['csapat_nev'],
    'tulajdonos' => $_POST['tulajdonos'],
    'cimer' => $_POST['cimer'],
    'becenev' => $_POST['becenev'],
    'bajnoksag_id' => $_POST['bajnok_id'],
];
$csapat->save($csapat_adatai);


$up_object_link = '/oop/csapatok_update_form.php';
$up_object_btn_name = 'Szerkesztés';

$up_csapat = new Csapatok();
$up_csapat_adatai = [
    'nev' => $_POST['up_csapat_nev'],
    'tulajdonos' => $_POST['up_tulajdonos'],
    'cimer' => $_POST['up_cimer'],
    'becenev' => $_POST['up_becenev'],
    'bajnoksag_id' => $_POST['up_bajnok_id'],
];
$up_csapat->update($up_csapat_adatai);


$crud_list = $csapat->getList();
$crud_list = $up_csapat->getList();

include "crud_table.php";