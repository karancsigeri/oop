<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2018. 09. 11.
 * Time: 18:57
 */
include "Db.php";
include "Bajnoksag.php";
include "Eredmeny.php";
include "Csapatok.php";
include "Jatekosok.php";



$bajnoksag = new Bajnoksag();
$bajnoksag_adatai = [
    'nev' => $_POST['bajnok_nev'],
    'orszag' => $_POST['orszag'],
    'szint' => $_POST['szint'],
    'foszponzor' => $_POST['szponzor'],
    'elnok' => $_POST['elnok'],
];
$bajnoksag->save($bajnoksag_adatai);


$eredmeny = new Eredmeny();
$eredmeny_adatai = [
    'hazai_csapat_id' => $_POST['hazai_id'],
    'vendeg_csapat_id' => $_POST['vendeg_id'],
    'hazai_golok_szama' => $_POST['hazai_gol'],
    'vendeg_golok_szama' => $_POST['vendeg_gol'],
];
$eredmeny->save($eredmeny_adatai);


$csapat = new Csapatok();
$csapat_adatai = [
    'nev' => $_POST['csapat_nev'],
    'tulajdonos' => $_POST['tulajdonos'],
    'cimer' => $_POST['cimer'],
    'becenev' => $_POST['becenev'],
    'bajnoksag_id' => $_POST['bajnok_id'],
];
$csapat->save($csapat_adatai);


$jatekos = new Jatekosok();
$jatekos_adatai = [
    'nev' => $_POST['jatekos_nev'],
    'csapat_id' => $_POST['csapat_id'],
];
$jatekos->save($jatekos_adatai);



/*
$updateles=new Bajnoksag();
$update1=[
    'id'=>6,
    'nev'=>"Slovan",
    'orszag'=>"Szlovákia",
    'szint'=>1,
    'foszponzok'=>"Marbolo",
    'elnok'=>"Jan Cseszlovaszky",
];
$updateles->update($update1);

*/

