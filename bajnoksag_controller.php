<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2018. 09. 11.
 * Time: 18:57
 */
header('Content-Type: text/html; charset=utf-8');

include "autoload.php";
$new_object_link = '/oop/bajnoksag_form.php';
$new_object_btn_name = 'Új bajnokság felvétele';
/*
$_POST['bajnok_nev'] = "";
$_POST['orszag'] = "";
$_POST['szint'] = "";
$_POST['szponzor'] = "";
$_POST['elnok'] = "";

$_POST['up_bajnok_nev']="";
$_POST['up_orszag']="";
$_POST['up_szint']="";
$_POST['up_szponzor']="";
$_POST['up_elnok']="";
*/
$bajnoksag = new Bajnoksag();
$bajnoksag_adatai = [
    'nev' => $_POST['bajnok_nev'],
    'orszag' => $_POST['orszag'],
    'szint' => $_POST['szint'],
    'foszponzor' => $_POST['szponzor'],
    'elnok' => $_POST['elnok'],
];
$bajnoksag->save($bajnoksag_adatai);


$up_object_link = '/oop/bajnoksag_update_form.php';
$up_object_btn_name = 'Szerkeszt';

$up_bajnoksag = new Bajnoksag();
$up_bajnoksag_adatai = [
    'nev' => $_POST['up_bajnok_nev'],
    'orszag' => $_POST['up_orszag'],
    'szint' => $_POST['up_szint'],
    'foszponzor' => $_POST['up_szponzor'],
    'elnok' => $_POST['up_elnok'],
];
$up_bajnoksag->update($up_bajnoksag_adatai);


$crud_list = $bajnoksag->getList();
$crud_list = $up_bajnoksag->getList();
include "crud_table.php";






