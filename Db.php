<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2018. 09. 11.
 * Time: 18:55
 */

class Db
{
    protected $mysqli;
    protected $db_tablename;

    /**
     * Db constructor.
     */
    public function __construct()
    {
        $this->mysqli = new mysqli("localhost", "root", "", "focivilag");
        if ($this->mysqli->connect_errno) {
            printf("Connect failed: %s\n", $this->mysqli->connect_error);
            exit();
        }
    }

    public function save($array)
    {
        if(isset($array['id']) && !empty($array['id'])){
            return $this->update($array);
        }else{
            return $this->insert($array);
        }
    }

    public function update($array)
    {
        if (isset($array['id'])) {
            // Megvizsgáljuk hogy létezik-eaz elem
            $sql_query = "SELECT COUNT(*) as darabszam FROM " . $this->db_tablename . " WHERE id='" . $array['id'] . "'";
            $res = $this->mysqli->query($sql_query);
            $talalt = false;
            if ($res->num_rows > 0) {
                while ($row = $res->fetch_assoc()) {
                    if (isset($row['darabszam']) && $row['darabszam'] == 1) {
                        $talalt = true;
                        break;
                    }
                }
            }
            if ($talalt) {
                //Befrissítem az adott sort
                $mysql_query = "UPDATE " . $this->db_tablename . " SET ";
                $keys = array_keys($array);
                if (!empty($keys)) {
                    $sub_query = "";
                    foreach ($keys as $key) {
                        if ($key !== 'id') {
                            $sub_query .= $key . "=\"" . $array[$key] . "\",";
                        }
                    }
                    $sub_query = trim($sub_query, ",");
                    $sub_query .= " WHERE id=" . $array['id'];
                    $mysql_query .= $sub_query;
                    return $this->mysqli->query($mysql_query);
                }
            }
        }
        return false;
    }

    public function insert($array){
        $keys = array_keys($array);
        if (!empty($keys)) {
            $sql_query = 'INSERT INTO ' . $this->db_tablename . ' (' . implode(", ", $keys) . ') values (';
            foreach ($array as $value) {
                $sql_query .= '"' . $value . '",';
            }
            $sql_query = trim($sql_query, ",");
            $sql_query .= ");";
            return $this->mysqli->query($sql_query);
        }
        return true;
    }

    public function delete($id, $vegleges_torles = false)
    {
        // Logikai törlés
        if ($vegleges_torles === false) {
            $update_array = [
                'id' => $id,
                'is_active' => 0,
                'updated_at' => date('Y-m-d H:i:s'),
                'deleted_at' => date('Y-m-d H:i:s'),
            ];
            return $this->update($update_array);
        } else {
            //Fizikai törlés
            $sql_query = "DELETE FROM " . $this->db_tablename . " WHERE id=" . $id;
            return $this->mysqli->query($sql_query);
        }
    }

//    public function getList($where=['id' => 5], $like=['id' => 'asd'], $order_by=['id' => 'DESC']){
    public function getList($where=[], $like=[], $order_by=[]){

        $mysql_query = "SELECT * FROM " . $this->db_tablename . ' WHERE is_active=1';
        $result = $this->mysqli->query($mysql_query);
        $return_array = [];
        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                $return_array[] = $row;
            }
        }
        return $return_array;
    }
}

