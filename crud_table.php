<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
      integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
      integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
<?php
if (isset($crud_list) && !empty($crud_list)) {
    $headers = array_keys($crud_list[0]);
    ?>
    <a href="<?= $new_object_link; ?>" class="btn btn-primary"><?= $new_object_btn_name; ?></a>
    <table class="table">
        <thead>
        <tr>
            <?php
            foreach ($headers as $header) {
                ?>
                <th scope="col"><?= $header; ?></th><?php
            }
            ?>
            <td><b>Szerkesztés</b></td>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($crud_list as $list_item) {
            ?>
            <tr>
                <?php
                foreach ($headers as $header) {
                    ?>
                    <td><?= $list_item[$header]; ?></td><?php
                }
                ?>
                <td><a href="<?= $up_object_link; ?>" class="btn btn-primary"><?= $up_object_btn_name; ?></a></td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
    <?php
}
?>