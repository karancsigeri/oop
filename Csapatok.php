<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2018. 08. 28.
 * Time: 18:25
 */

class Csapatok extends Db
{

    protected $id;
    protected $nev;
    protected $tulajdonos;
    protected $cimer;
    protected $becenev;
    protected $bajnoksag_id;
    protected $jatekosok=[];




    /**
     * Csapatok constructor.
     * @param $id
     */
    public function __construct()
    {
        $this->db_tablename = "csapatok";
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNev()
    {
        return $this->nev;
    }

    /**
     * @param mixed $nev
     */
    public function setNev($nev)
    {
        $this->nev = $nev;
    }

    /**
     * @return mixed
     */
    public function getTulajdonos()
    {
        return $this->tulajdonos;
    }

    /**
     * @param mixed $tulajdonos
     */
    public function setTulajdonos($tulajdonos)
    {
        $this->tulajdonos = $tulajdonos;
    }

    /**
     * @return mixed
     */
    public function getCimer()
    {
        return $this->cimer;
    }

    /**
     * @param mixed $cimer
     */
    public function setCimer($cimer)
    {
        $this->cimer = $cimer;
    }

    /**
     * @return mixed
     */
    public function getBecenev()
    {
        return $this->becenev;
    }

    /**
     * @param mixed $becenev
     */
    public function setBecenev($becenev)
    {
        $this->becenev = $becenev;
    }

    /**
     * @return mixed
     */
    public function getBajnoksagId()
    {
        return $this->bajnoksag_id;
    }

    /**
     * @param mixed $bajnoksag_id
     */
    public function setBajnoksagId($bajnoksag_id)
    {
        $this->bajnoksag_id = $bajnoksag_id;
    }

    /**
     * @return array
     */
    public function getJatekosok()
    {
        return $this->jatekosok;
    }

    /**
     * @param array $jatekosok
     */
    public function setJatekosok($jatekosok)
    {
        $this->jatekosok = $jatekosok;
    }



}