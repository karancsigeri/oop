<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2018. 08. 28.
 * Time: 18:04
 */
class Bajnoksag extends Db
{
    protected $id;
    protected $nev;
    protected $orszag;
    protected $szint;
    protected $foszponzor;
    protected $elnok;
    protected $csapatok=[];
    protected $para_bajnoksag=[];
    protected $csapat_id;


    /**
     *
     */
    public function test(){
        var_dump(get_object_vars($this));
    }

    /**
     * Bajnoksag constructor.
     * @param $id
     * @param $nev
     * @param $orszag
     * @param $szint
     * @param $foszponzor
     * @param $elnok
     */
    public function __construct()
    {
        $this->db_tablename = 'bajnoksag';
        parent::__construct();


    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNev()
    {
        return $this->nev;
    }

    /**
     * @param mixed $nev
     */
    public function setNev($nev)
    {
        $this->nev = $nev;
    }

    /**
     * @return mixed
     */
    public function getOrszag()
    {
        return $this->orszag;
    }

    /**
     * @param mixed $orszag
     */
    public function setOrszag($orszag)
    {
        $this->orszag = $orszag;
    }

    /**
     * @return mixed
     */
    public function getSzint()
    {
        return $this->szint;
    }

    /**
     * @param mixed $szint
     */
    public function setSzint($szint)
    {
        $this->szint = $szint;
    }

    /**
     * @return mixed
     */
    public function getFoszponzor()
    {
        return $this->foszponzor;
    }

    /**
     * @param mixed $foszponzor
     */
    public function setFoszponzor($foszponzor)
    {
        $this->foszponzor = $foszponzor;
    }

    /**
     * @return mixed
     */
    public function getElnok()
    {
        return $this->elnok;
    }

    /**
     * @param mixed $elnok
     */
    public function setElnok($elnok)
    {
        $this->elnok = $elnok;
    }

    /**
     * @return array
     */
    public function getCsapatok()
    {
        return $this->csapatok;
    }

    /**
     * @param array $csapatok
     */
    public function setCsapatok($csapatok)
    {
        $this->csapatok = $csapatok;
    }

    /**
     * @return array
     */
    public function getParaBajnoksag()
    {
        return $this->para_bajnoksag;
    }

    /**
     * @param array $para_bajnoksag
     */
    public function setParaBajnoksag($para_bajnoksag)
    {
        $this->para_bajnoksag = $para_bajnoksag;
    }

    /**
     * @return mixed
     */
    public function getCsapatId()
    {
        return $this->csapat_id;
    }

    /**
     * @param mixed $csapat_id
     */
    public function setCsapatId($csapat_id)
    {
        $this->csapat_id = $csapat_id;
    }

    public function createObject($id, $nev, $orszag, $szint, $foszponzor, $elnok){
        $this->id = $id;
        $this->nev= $nev;
        $this->orszag = $orszag;
        $this->szint = $szint;
        $this->foszponzor = $foszponzor;
    }

    public function getCsapatById($id){
        if(!empty($this->csapatok)){
            foreach ($this->csapatok as $csapat){
                if($csapat instanceof Csapatok){
                    if($csapat->getId() == $id){
                        return $csapat;
                    }
                }
            }
        }
        return false;
    }
}