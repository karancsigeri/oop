<?php
/**
 * Created by PhpStorm.
 * User: Qosmio_X770-10X
 * Date: 2018.09.19.
 * Time: 9:28
 */

header('Content-Type: text/html; charset=utf-8');

include "autoload.php";
$new_object_link = '/oop/eredmeny_form.php';
$new_object_btn_name = 'Új eredmények felvétele';
/*
$_POST['hazai_id'] = "";
$_POST['vendeg_id'] = "";
$_POST['hazai_gol'] = "";
$_POST['vendeg_gol'] = "";

$_POST['up_hazai_id']="";
$_POST['up_vendeg_id']="";
$_POST['up_hazai_gol']="";
$_POST['up_vendeg_gol']="";
*/

$eredmeny = new Eredmeny();
$eredmeny_adatai = [
    'hazai_csapat_id' => $_POST['hazai_id'],
    'vendeg_csapat_id' => $_POST['vendeg_id'],
    'hazai_golok_szama' => $_POST['hazai_gol'],
    'vendeg_golok_szama' => $_POST['vendeg_gol'],
];
$eredmeny->save($eredmeny_adatai);


$up_object_link = '/oop/eredmenyek_update_form.php';
$up_object_btn_name = 'Szerkeszt';

$up_eredmeny = new Eredmeny();
$up_eredmeny_adatai = [
    'hazai_csapat_id' => $_POST['up_hazai_id'],
    'vendeg_csapat_id' => $_POST['up_vendeg_id'],
    'hazai_golok_szama' => $_POST['up_hazai_gol'],
    'vendeg_golok_szama' => $_POST['up_vendeg_gol'],
];
$up_eredmeny->update($up_eredmeny_adatai);


$crud_list = $eredmeny->getList();
$crud_list = $up_eredmeny->getList();
include "crud_table.php";